﻿Imports System.Windows.Forms
Imports System.IO

Public Class dlgSettings
    Private Sub dlgSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ini As String()
        Dim folder, name As String
        name = Main.lbClones.SelectedItem
        Me.Text = "mTES4: Clone Settings for '" & name & "'"
        folder = name
        If String.Equals(folder, Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
            folder = Main.InstallPath
        End If
        ini = ini_load(Get_GamePath(folder & "\_mTES4 (" & name & ").ini"))
        tbTitle.Text = ini_get(ini, "AutoLaunch", "sTitle", "")
        tbPath.Text = ini_get(ini, "AutoLaunch", "sFilePath", "")
        tbArgs.Text = ini_get(ini, "AutoLaunch", "sArguments", "")
        cbAlways.Checked = String.Equals(ini_get(ini, "AutoLaunch", "bLaunchAlways", "FALSE").ToUpper(), "TRUE")
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Dim ini As String()
        Dim folder, name As String
        name = IO.Path.Combine(Get_GamePath(Main.InstallPath), tbPath.Text)
        If Not File.Exists(name) Then
            MsgBox("Invalid Path Specified!")
            Me.DialogResult = Windows.Forms.DialogResult.None
            Exit Sub
        End If
        name = Main.lbClones.SelectedItem
        folder = name
        If String.Equals(folder, Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
            folder = Main.InstallPath
        End If
        ini = ini_load(Get_GamePath(folder & "\_mTES4 (" & name & ").ini"))
        ini = ini_set(ini, "AutoLaunch", "sTitle", tbTitle.Text)
        ini = ini_set(ini, "AutoLaunch", "sFilePath", tbPath.Text)
        ini = ini_set(ini, "AutoLaunch", "sArguments", tbArgs.Text)
        ini = ini_set(ini, "AutoLaunch", "bLaunchAlways", IIf(cbAlways.Checked, "True", "False"))
        ini_save(Get_GamePath(folder & "\_mTES4 (" & name & ").ini"), ini)
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim dlgFile As New OpenFileDialog()
        Dim gpath As String

        dlgFile.AddExtension = False
        dlgFile.CheckFileExists = True
        dlgFile.CheckPathExists = True
        dlgFile.Multiselect = False
        dlgFile.RestoreDirectory = True
        If Not tbPath.Text.Equals("") Then
            dlgFile.InitialDirectory = Get_GamePath(tbPath.Text)
        ElseIf String.Equals(Main.lbClones.SelectedItem, Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
            dlgFile.InitialDirectory = Get_GamePath(Main.InstallPath)
        Else
            dlgFile.InitialDirectory = Get_GamePath(Main.lbClones.SelectedItem)
        End If
        dlgFile.Title = "Select the tool to autolaunch."
        If dlgFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
            gpath = Get_GamePath(Main.InstallPath).ToLower()
            tbPath.Text = IO.Path.Combine(gpath, dlgFile.FileName)
            If tbPath.Text.ToLower().StartsWith(gpath) Then
                tbPath.Text = tbPath.Text.Substring(gpath.Length + 1)
            End If
        End If
    End Sub
End Class
