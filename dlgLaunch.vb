﻿Imports System.Windows.Forms
Imports System.IO

Public Class dlgLaunch
    Private Tools As System.Collections.Generic.List(Of Shortcut)

    Private Sub AddTool(ByVal name As String, ByVal path As String, Optional ByVal args As String = "", Optional ByVal icon As String = "")
        If Not File.Exists(path) Then Exit Sub

        Tools.Add(New Shortcut(path, args))

        If icon.Equals("") Then icon = path
        ilTools16.Images.Add(name, System.Drawing.Icon.ExtractAssociatedIcon(icon))
        ilTools32.Images.Add(name, System.Drawing.Icon.ExtractAssociatedIcon(icon))

        lvTools.Items.Add(name, name)
    End Sub

    Private Sub AddCustomTools(ByRef tools As System.Collections.Generic.List(Of Shortcut), ByVal file As String, ByVal section As String)
        Dim ini As String()
        Dim f, offset As Integer
        Dim name, path, args, icon As String

        ini = ini_load(file)
        offset = 0
        Do
            f = ini_find(ini, section, "", offset)
            If f = -1 Then Exit Do
            offset = f

            name = ini_get(ini, section, "sTitle", "", offset)
            path = ini_get(ini, section, "sFilePath", "", offset)
            args = ini_get(ini, section, "sArguments", "", offset)
            icon = ini_get(ini, section, "sIconPath", "", offset)
            If Not path.Equals("") Then path = IO.Path.Combine(Get_GamePath(Main.InstallPath), path)
            If Not icon.Equals("") Then icon = IO.Path.Combine(Get_GamePath(Main.InstallPath), icon)
            AddTool(name, path, args, icon)
            offset += 1
        Loop
    End Sub

    Private Sub dlgLaunch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim game As String

        Tools = New System.Collections.Generic.List(Of Shortcut)
        ilTools16.Images.Clear()
        ilTools32.Images.Clear()
        lvTools.Clear()

        game = Main.cbGames.SelectedItem
        If game.ToLower().Equals("oblivion") Then
            AddTool("OBSE + " & game, Get_GamePath(Main.InstallPath & "\obse_loader.exe"), "", Get_GamePath(Main.InstallPath & "\" & game & ".exe"))
            If File.Exists(Get_GamePath(Main.InstallPath & "\TESConstructionSet.exe")) Then
                AddTool("OBSE + TES4CS", Get_GamePath(Main.InstallPath & "\obse_loader.exe"), "-editor", Get_GamePath(Main.InstallPath & "\TESConstructionSet.exe"))
            End If
        ElseIf game.ToLower().Equals("fallout3") Or game.ToLower().Equals("falloutnv") Then
            AddTool("FOSE + " & game, Get_GamePath(Main.InstallPath & "\fose_loader.exe"), "", Get_GamePath(Main.InstallPath & "\" & game & ".exe"))
            If File.Exists(Get_GamePath(Main.InstallPath & "\Geck.exe")) Then
                AddTool("FOSE + GECK", Get_GamePath(Main.InstallPath & "\fose_loader.exe"), "-editor", Get_GamePath(Main.InstallPath & "\Geck.exe"))
            End If
        End If

        AddCustomTools(Tools, Get_AppPath("mTES4.ini"), game & " Tool")
        AddCustomTools(Tools, Get_AppPath("mTES4.ini"), "Custom Tool")
        AddCustomTools(Tools, Get_GamePath(Main.InstallPath & "\_mTES4 (" & Main.TrackActive & ").ini"), "Custom Tool")

        lvTools.SmallImageList = ilTools16
        lvTools.LargeImageList = ilTools32

        lvTools.View = My.Settings.LaunchDlgView
        miIcons.Checked = (lvTools.View = View.LargeIcon)
        miList.Checked = (lvTools.View = View.List)

        btnLaunch.Enabled = False
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub miTiles_Click(ByVal item As ToolStripMenuItem, ByVal e As System.EventArgs) Handles miIcons.Click, miList.Click
        miIcons.Checked = False
        miList.Checked = False
        Select Case item.Text
            Case "Icons"
                lvTools.View = View.LargeIcon
            Case "List"
                lvTools.View = View.List
        End Select
        My.Settings.LaunchDlgView = lvTools.View
        item.Checked = True
    End Sub

    Private Sub lvTools_ItemActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTools.ItemActivate
        Launch()
    End Sub

    Private Class Shortcut
        Public Path, Args, WorkingDirectory As String

        Public Sub New(ByVal path As String, ByVal args As String)
            Dim dirinfo As DirectoryInfo
            Me.Path = New String(path)
            Me.Args = New String(args)
            dirinfo = My.Computer.FileSystem.GetDirectoryInfo(Me.Path)
            Me.WorkingDirectory = dirinfo.Parent.FullName
        End Sub
    End Class

    Private Sub Launch()
        Dim startInfo As New ProcessStartInfo()
        Dim index As Integer

        index = lvTools.SelectedIndices.Item(0)
        startInfo.FileName = Tools.Item(index).Path
        startInfo.Arguments = Tools.Item(index).Args
        startInfo.WorkingDirectory = Tools.Item(index).WorkingDirectory
        Process.Start(startInfo)

        Main.WindowState = FormWindowState.Minimized
        Me.Close()
    End Sub

    Private Sub btnLaunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLaunch.Click
        Launch()
    End Sub

    Private Sub lvTools_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTools.SelectedIndexChanged
        btnLaunch.Enabled = (lvTools.SelectedIndices.Count > 0)
    End Sub
End Class
