﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dlgSettings))
        Me.btnApply = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.tbPath = New System.Windows.Forms.TextBox
        Me.gbAutoLaunch = New System.Windows.Forms.GroupBox
        Me.cbAlways = New System.Windows.Forms.CheckBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblPath = New System.Windows.Forms.Label
        Me.tbArgs = New System.Windows.Forms.TextBox
        Me.tbTitle = New System.Windows.Forms.TextBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gbAutoLaunch.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.Location = New System.Drawing.Point(317, 125)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(67, 23)
        Me.btnApply.TabIndex = 2
        Me.btnApply.Text = "&Apply"
        Me.ToolTip1.SetToolTip(Me.btnApply, "Apply the changes and return to the main mTES4 window.")
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(244, 125)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(67, 23)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.ToolTip1.SetToolTip(Me.btnCancel, "Cancel any changes and return to the main mTES4 window.")
        '
        'tbPath
        '
        Me.tbPath.Location = New System.Drawing.Point(47, 43)
        Me.tbPath.Name = "tbPath"
        Me.tbPath.Size = New System.Drawing.Size(238, 20)
        Me.tbPath.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.tbPath, "The path to the executable file for this tool." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This can be an absolute path or i" & _
                "t can be relative to the game folder.")
        '
        'gbAutoLaunch
        '
        Me.gbAutoLaunch.Controls.Add(Me.cbAlways)
        Me.gbAutoLaunch.Controls.Add(Me.btnBrowse)
        Me.gbAutoLaunch.Controls.Add(Me.Label1)
        Me.gbAutoLaunch.Controls.Add(Me.lblTitle)
        Me.gbAutoLaunch.Controls.Add(Me.lblPath)
        Me.gbAutoLaunch.Controls.Add(Me.tbArgs)
        Me.gbAutoLaunch.Controls.Add(Me.tbTitle)
        Me.gbAutoLaunch.Controls.Add(Me.tbPath)
        Me.gbAutoLaunch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbAutoLaunch.Location = New System.Drawing.Point(12, 12)
        Me.gbAutoLaunch.Name = "gbAutoLaunch"
        Me.gbAutoLaunch.Size = New System.Drawing.Size(372, 99)
        Me.gbAutoLaunch.TabIndex = 0
        Me.gbAutoLaunch.TabStop = False
        Me.gbAutoLaunch.Text = "Auto Launch"
        Me.ToolTip1.SetToolTip(Me.gbAutoLaunch, "Defines a tool that will be automatically launched whenever switching to the rela" & _
                "ted clone.")
        '
        'cbAlways
        '
        Me.cbAlways.AutoSize = True
        Me.cbAlways.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbAlways.Location = New System.Drawing.Point(265, 19)
        Me.cbAlways.Name = "cbAlways"
        Me.cbAlways.Size = New System.Drawing.Size(101, 17)
        Me.cbAlways.TabIndex = 0
        Me.cbAlways.Text = "&Always Launch:"
        Me.ToolTip1.SetToolTip(Me.cbAlways, resources.GetString("cbAlways.ToolTip"))
        Me.cbAlways.UseVisualStyleBackColor = True
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(291, 42)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 3
        Me.btnBrowse.Text = "&Browse"
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for the tool path.")
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Args:"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Location = New System.Drawing.Point(9, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(30, 13)
        Me.lblTitle.TabIndex = 1
        Me.lblTitle.Text = "Title:"
        '
        'lblPath
        '
        Me.lblPath.AutoSize = True
        Me.lblPath.Location = New System.Drawing.Point(9, 47)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(32, 13)
        Me.lblPath.TabIndex = 1
        Me.lblPath.Text = "Path:"
        '
        'tbArgs
        '
        Me.tbArgs.Location = New System.Drawing.Point(47, 69)
        Me.tbArgs.Name = "tbArgs"
        Me.tbArgs.Size = New System.Drawing.Size(238, 20)
        Me.tbArgs.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.tbArgs, "If you need to specify any command line arguments for the tool.")
        '
        'tbTitle
        '
        Me.tbTitle.Location = New System.Drawing.Point(47, 17)
        Me.tbTitle.Name = "tbTitle"
        Me.tbTitle.Size = New System.Drawing.Size(156, 20)
        Me.tbTitle.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.tbTitle, "This is the name of the tool that wil be used to refer to the tool that will be a" & _
                "uto-launched." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If no Title is specified, the file name will be used instead.")
        '
        'dlgSettings
        '
        Me.AcceptButton = Me.btnApply
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(396, 160)
        Me.Controls.Add(Me.gbAutoLaunch)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnApply)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgSettings"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "mTES4: Clone Settings for 'NAME'"
        Me.gbAutoLaunch.ResumeLayout(False)
        Me.gbAutoLaunch.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents tbPath As System.Windows.Forms.TextBox
    Friend WithEvents gbAutoLaunch As System.Windows.Forms.GroupBox
    Friend WithEvents cbAlways As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblPath As System.Windows.Forms.Label
    Friend WithEvents tbArgs As System.Windows.Forms.TextBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents tbTitle As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class
